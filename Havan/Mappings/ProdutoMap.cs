﻿using Havan.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Havan.Mappings
{
    public class ProdutoMap : IEntityTypeConfiguration<Produto>
    {
        public void Configure(EntityTypeBuilder<Produto> builder)
        {
            builder.ToTable("produto");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
                .HasColumnName("id")
                .ValueGeneratedOnAdd();

            builder.Property(x => x.Preco)
                .HasColumnName("preco")
                .HasColumnType("money");

            builder.Property(x => x.Sku)
                .HasColumnName("sku")
                .HasMaxLength(20)
                .IsUnicode(false);

            builder.Property(x => x.Nome)
                .HasColumnName("nome")
                .HasMaxLength(120)
                .IsUnicode(false);

            builder.Property(x => x.UrlImagem)
                .HasColumnName("urlimagem")
                .HasMaxLength(120)
                .IsUnicode(false);

            builder.Property(x => x.IdFamilia)
                .HasColumnName("idfamilia");

            builder.HasOne(x => x.Familia)
                .WithMany(p => p.Produto)
                .HasForeignKey(x => x.IdFamilia)
                .HasConstraintName("fk_produto_01");
        }
    }
}
