﻿using Havan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Havan.Interfaces
{
    public interface IFamiliaService
    {
        IEnumerable<Familia> SelectAll();
        Familia SelectById(int id);
    }
}
