﻿using Havan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Havan.Interfaces
{
    public interface IProdutoRepositorio
    {
        IEnumerable<Produto> SelectAll();
        IEnumerable<Produto> SelectAllOrderFamilia();
        IEnumerable<Produto> SelectByIdFamilia(int id);
        Produto SelectById(int id);
    }
}
