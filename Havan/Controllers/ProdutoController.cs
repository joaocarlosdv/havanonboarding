﻿using Havan.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Havan.Controllers
{
    [ApiController]
    [Route("api/produto")]
    public class ProdutoController : Controller
    {
        private readonly IProdutoService produtoService;

        public ProdutoController(IProdutoService _produtoService)
        {
            produtoService = _produtoService;
        }        

        [HttpGet("selectall")]
        public ActionResult SelectAll()
        {
            return Ok(produtoService.SelectAll());
        }

        [HttpGet("selectallorderfamilia")]
        public ActionResult SelectAllOrderFamilia()
        {
            return Ok(produtoService.SelectAllOrderFamilia());
        }

        [HttpGet("selectbyidfamilia/{id}")]
        public ActionResult SelectByIdFamilia(int id)
        {
            return Ok(produtoService.SelectByIdFamilia(id));
        }

        [HttpGet("selectbyid/{id}")]
        public ActionResult SelectById(int id)
        {
            return Ok(produtoService.SelectById(id));
        }
    }
}
