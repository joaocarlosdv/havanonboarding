﻿using Havan.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Havan.Controllers
{
    [ApiController]
    [Route("api/familia")]
    public class FamiliaController : Controller
    {
        private readonly IFamiliaService familiaService;

        public FamiliaController(IFamiliaService _familiaService)
        {
            familiaService = _familiaService;
        }

        [HttpGet("selectall")]
        public ActionResult SelectAll()
        {
            return Ok(familiaService.SelectAll());
        }

        [HttpGet("selectbyid/{id}")]
        public ActionResult SelectById(int id)
        {
            return Ok(familiaService.SelectById(id));
        }
    }
}
