﻿using Havan.Interfaces;
using Havan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Havan.Services
{
    public class FamiliaService : IFamiliaService
    {
        private readonly IFamiliaRepositorio familiaRepositorio;

        public FamiliaService
        (
            IFamiliaRepositorio _familiaRepositorio
        )
        {
            familiaRepositorio = _familiaRepositorio;
        }
        public IEnumerable<Familia> SelectAll()
        {
            return familiaRepositorio.SelectAll();
        }

        public Familia SelectById(int id)
        {
            return familiaRepositorio.SelectById(id);
        }
    }
}
