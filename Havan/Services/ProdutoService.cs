﻿using Havan.Interfaces;
using Havan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Havan.Services
{
    public class ProdutoService : IProdutoService
    {
        private readonly IProdutoRepositorio produtoRepositorio;

        public ProdutoService
        (
            IProdutoRepositorio _produtoRepositorio
        )
        {
            produtoRepositorio = _produtoRepositorio;
        }

        public IEnumerable<Produto> SelectAll()
        {
            return produtoRepositorio.SelectAll();
        }

        public IEnumerable<Produto> SelectAllOrderFamilia()
        {
            return produtoRepositorio.SelectAllOrderFamilia();
        }

        public IEnumerable<Produto> SelectByIdFamilia(int id)
        {
            return produtoRepositorio.SelectByIdFamilia(id);
        }

        public Produto SelectById(int id)
        {
            return produtoRepositorio.SelectById(id);
        }
    }
}
