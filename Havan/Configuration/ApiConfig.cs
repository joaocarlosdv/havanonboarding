﻿using Havan.Interfaces;
using Havan.Repositories;
using Havan.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Havan.Configuration
{
    public static class ApiConfig
    {
        public static void ApiConfigServices(this IServiceCollection services, IConfiguration configuration)
        {
            #region Services
            services.AddScoped<IProdutoService, ProdutoService>();
            services.AddScoped<IFamiliaService, FamiliaService>();
            #endregion

            #region Repositories
            services.AddScoped<IProdutoRepositorio, ProdutoRepositorio>();
            services.AddScoped<IFamiliaRepositorio, FamiliaRepositorio>();
            #endregion
        }
    }
}

