﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Havan.Models
{
    public class Familia
    {
        public int Id { get; set; }
        public string Nome { get; set; }

        public ICollection<Produto> Produto { get; set; }
    }
}
