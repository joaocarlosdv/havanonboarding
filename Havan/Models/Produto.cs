﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Havan.Models
{
    public class Produto
    {
        public int Id { get; set; }
        public decimal Preco { get; set; }
        public string Sku { get; set; }
        public string Nome { get; set; }
        public string UrlImagem { get; set; }
        public int IdFamilia { get; set; }
        public Familia Familia { get; set; }
    }
}
