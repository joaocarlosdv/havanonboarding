﻿using Havan.Context;
using Havan.Interfaces;
using Havan.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Havan.Repositories
{
    public class ProdutoRepositorio : IProdutoRepositorio
    {
        private readonly ApiContext _db;
        public ProdutoRepositorio(ApiContext db)
        {
            _db = db;
        }

        public IEnumerable<Produto> SelectAll()
        {
            return 
             _db.Produto
                .Include(p=>p.Familia)
                .OrderBy(p => p.Nome)
                .AsNoTracking()
                .ToList();
        }

        public IEnumerable<Produto> SelectAllOrderFamilia()
        {
            return 
             _db.Produto
                .Include(p => p.Familia)
                .OrderBy(p => p.Familia.Nome)
                .AsNoTracking()
                .ToList();
        }

        public IEnumerable<Produto> SelectByIdFamilia(int id)
        {
            return _db.Produto
                .Include(p => p.Familia)
                .Where(p=>p.Familia.Id == id)
                .OrderBy(p => p.Nome)
                .AsNoTracking()
                .ToList();
        }

        public Produto SelectById(int id)
        {
            return _db.Produto
                .Include(p => p.Familia)
                .Where(p => p.Id == id)
                .AsNoTracking()
                .FirstOrDefault();
        }      


    }
}
