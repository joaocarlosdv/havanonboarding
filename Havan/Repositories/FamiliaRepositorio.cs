﻿using Havan.Context;
using Havan.Interfaces;
using Havan.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Havan.Repositories
{
    public class FamiliaRepositorio : IFamiliaRepositorio
    {
        private readonly ApiContext _db;
        public FamiliaRepositorio(ApiContext db)
        {
            _db = db;
        }

        public IEnumerable<Familia> SelectAll()
        {
            return _db.Familia
                .OrderBy(p => p.Nome)
                .AsNoTracking()
                .ToList();
        }

        public Familia SelectById(int id)
        {
            return _db.Familia
                .Where(p => p.Id == id)
                .AsNoTracking()
                .FirstOrDefault();
        }
    }
}
